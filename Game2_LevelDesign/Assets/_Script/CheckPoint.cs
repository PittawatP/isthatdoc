﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            GameManeger.instant.SavePoint.x = transform.position.x;

            GameManeger.instant.SavePoint.y = transform.position.y;

            GameManeger.instant.SavePoint.z = transform.position.z;
        }


    }
}
