﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManeger : MonoBehaviour
{
    [SerializeField]
    public int HPPlayer = 100;
    [SerializeField]
    public int Score { get; set; }
    public int Life = 2;
    [SerializeField]
    public int Temperature=1;
    //G Y O R

    [SerializeField]
    private GameObject Panel;
    [SerializeField]
    private Button BacktoMenu;
    [SerializeField]
    private Button BacktoStage;

    [SerializeField]
    public int Ammo { get; set; }

    public static GameManeger instant { get;private set; }
    public bool MusicEnabled { get; set; } = true;

    public bool SFXEnabled { get; set; } = true;

    public bool isFaceOn = false;
    public int Faceprotect = 3;

    [SerializeField]
    public int Pill { get; set; }

    public Vector3 SavePoint;
    [SerializeField]
    private GameObject Player;

    [SerializeField]
    private GameObject UIgame;
    public bool isDead = false;
    public bool deadSFXplay = false;
    [SerializeField]
    public bool GameStart = false;
    [SerializeField]
    public bool IsGameEnd = false;
    [SerializeField]
    private Animator anim;
    public bool lv1 = true;
    public bool lv2 = false;
    public bool lv3 = false;
    [SerializeField]
    private Image hp;
    public Text AmmoText;
    public Text PillText;
    public Text Scoretxt;
    public Text TimeText;
    public Text LiftText;

    private float Timeee;
    private int TimeMin;

    [SerializeField]
    private GameObject UIPause;
    public bool isPause=false;
    [SerializeField]
    private Button BacktoMenu1;
    [SerializeField]
    private Button BacktoStage1;
    [SerializeField]
    private Button UnpauseButton;


    [SerializeField]
    private GameObject UIENDGAME;
    [SerializeField]
    private Button BacktoMenu2;
    [SerializeField] public AudioSource audiosourceSFX;
    [SerializeField] AudioClip audioclipDead;
    private void Awake()
    {
        if (instant == null)
        {
            instant = this;
            DontDestroyOnLoad(gameObject);

        }
        else
        {
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        instant = this;
        Ammo = 10;
        Pill = 1;
        Panel.SetActive(false);
        SetupDelegate();
        isDead = false;


    }

    // Update is called once per frame
    void Update()
    {   if (!GameStart) { 
            UIgame.SetActive(false);
            UIPause.SetActive(false);
            UIENDGAME.SetActive(false);
        }
        else { 
            UIgame.SetActive(true);
       if(isPause)
            {
                //Cursor.visible = true;
            }
            //else Cursor.visible = false;
            if (IsGameEnd)
            {
                UIENDGAME.SetActive(true);

            }
            else if(!IsGameEnd) 
                UIENDGAME.SetActive(false);

            if (isDead)
            {

                Panel.SetActive(true);
                
                //Cursor.visible = true ;
                if (!audiosourceSFX.isPlaying && deadSFXplay)
                {
                    audiosourceSFX.PlayOneShot(audioclipDead);
                    deadSFXplay = false;
                    HPPlayer = 1;
                }
                


            }
            else { Panel.SetActive(false); }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (isPause)
                {
                    isPause = false;
                }
                else  isPause = true; 
            }

            if (isPause)
            {
                UIPause.SetActive(true);
            }
            else UIPause.SetActive(false);

            if (Timeee<60)
            {
            Timeee += Time.deltaTime;
            }
           else
            {
                TimeMin++;
                Timeee = 0;
            }

            TimeText.text ="Time :"+TimeMin.ToString()+" :M  "+ Timeee.ToString("0")+" :S";
            LiftText.text = "X " + Life.ToString();
        }
            
        anim.SetInteger("Temp", Temperature);
        Player = GameObject.Find("Player");
        Scoretxt.text ="Score :"+ Score.ToString();

        //if (SavePoint == null)
        //{
        //    SavePoint = Player.transform.position;
        //}
        hp.fillAmount = (float)HPPlayer / 100;
        if (HPPlayer>=100)
        { HPPlayer = 100; 
        }

        if (Temperature <= 1)
            Temperature = 1;
        if (Temperature >= 4)
            Temperature = 4;
        PillText.text = "X " + Pill.ToString();
        AmmoText.text ="X  "+ Ammo.ToString();

        if (Pill<0)
        { Pill = 0; }
        if(HPPlayer<=0)
        {
            
            if (Life > 0)
            {
                Life--;
                HPPlayer = 100;
                Temperature = 1;
                Player.transform.position = SavePoint;
                isDead = false;
            }
            else
            {
                isDead = true;
                deadSFXplay = true;
                
            }
        }
        if(Faceprotect<=0)
        {
            isFaceOn = false;
        }
   
     

    }
    void SetupDelegate()
    {
        BacktoMenu.onClick.AddListener(delegate { MenuClick(BacktoMenu); });

        BacktoStage.onClick.AddListener(delegate { StageClick(BacktoStage); });
        BacktoMenu1.onClick.AddListener(delegate { MenuClick(BacktoMenu1); });
        BacktoMenu2.onClick.AddListener(delegate { MenuClick(BacktoMenu2); });

        BacktoStage1.onClick.AddListener(delegate { StageClick(BacktoStage1); });
        UnpauseButton.onClick.AddListener(delegate { Unpause(UnpauseButton); });

    }
    void MenuClick(Button button)
    {
        SceneManager.LoadScene("MainMenu");
        Panel.SetActive(false);
        Resetto();
    }
    void StageClick(Button button)
    {
        SceneManager.LoadScene("StageSelect");
        Panel.SetActive(false);
        Resetto();
    }
    private void Resetto()
    {
        HPPlayer = 100;
        Life = 2;
        isDead = false;
        isPause = false;
        Temperature = 1;
        Ammo = 10;
        Pill = 1;
        Score = 0;
        TimeMin = 0;
        Timeee = 0;
        IsGameEnd = false;
        GameStart = false;
    }

    void Unpause(Button button)
    {

        isPause = false;
      
    }
    
   


}
