﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoBox : MonoBehaviour
{
    [SerializeField]
    private int PlusAmmo = 20;
    private AudioSource ammopickAS;

    private void Start()
    {
        ammopickAS = GameObject.Find("SFXpickUpAmmo").GetComponent<AudioSource>();


    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            ammopickAS.Play();
            Destroy(gameObject);
            GameManeger.instant.Ammo += PlusAmmo;
        }
    }
}
