﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Virus : MonoBehaviour
{
    [SerializeField]
    private GameObject Target;
    [SerializeField]
    private GameObject HitBox;
    [SerializeField]
    private LayerMask PlayerLayer;
    [SerializeField]
    private bool isAttack = false;
    [SerializeField]
    private Rigidbody rb;
    [SerializeField]
    private float Speed;
    [SerializeField]
    private int DMG=1;
    [SerializeField]
    private ParticleSystem VirusPS;


    [SerializeField]
    private GameObject AmmoItem;


    [SerializeField]
    private GameObject PillItem;
    private void Start()
    {
        rb.GetComponent<Rigidbody>();
        Target  = GameObject.Find("Player");
    }
    void MyCollisions()
    {
        //Use the OverlapBox to detect if there are any other colliders within this box area.
        //Use the GameObject's centre, half the size (as a radius) and rotation. This creates an invisible box around your GameObject.
        Collider[] hitColliders = Physics.OverlapBox(HitBox.transform.position, HitBox.transform.localScale / 2, Quaternion.identity, PlayerLayer);
        int i = 0;
        //Check when there is a new collider coming into contact with the box
        while (i < hitColliders.Length)
        {
            //Output all of the collider names
          
            if (hitColliders[i].CompareTag("Player"))
            {
                isAttack = true;


            }


            //Increase the number of Colliders in the array
            i++;
        }

    }
    private void Update()
    {
        MyCollisions();
        if (isAttack)
        {
            rb.AddForce((Target.transform.position - transform.position).normalized * Speed* Time.deltaTime, ForceMode.Impulse);
            VirusPS.Play();
        }
        
    }

   
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {if(GameManeger.instant.Faceprotect>0&&GameManeger.instant.isFaceOn)
            {
                GameManeger.instant.Faceprotect--;
            }
        else
            GameManeger.instant.HPPlayer-=DMG;
            Destroy(gameObject);

        }
        if (collision.gameObject.CompareTag("Bullet"))
        {
            Destroy(gameObject);
            int r = Random.RandomRange(0, 100);
            if(r<50)
            {

            }if(r>50&&r<=75)
            {
                Instantiate(AmmoItem, transform.position, Quaternion.identity);
            }
            if (r > 75 && r < 100)
            {
                Instantiate(PillItem, transform.position,Quaternion.identity);
            }

        }

    }

}
