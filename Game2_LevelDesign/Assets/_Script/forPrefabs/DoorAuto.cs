﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorAuto : MonoBehaviour
{
    [SerializeField]
    private KeyCode key;
    [SerializeField]
    private GameObject Player;
    [SerializeField]
    private GameObject Door;

    [SerializeField]
    private float timeDelay;
    private float startTime;
    private bool isStart;
    [SerializeField]
    private int temper = 2;
    [SerializeField]
    private GameObject Topoint;
    // Start is called before the first frame update
    void Start()
    {
        startTime = timeDelay;
    }

    // Update is called once per frame
    void Update()
    {   if(isStart)
        if (timeDelay > 0)
        {
            timeDelay -= Time.deltaTime;
            Door.transform.position += Vector3.right * Time.deltaTime;
        }
        else
        {
                Player.transform.position = Topoint.transform.position;
                isStart = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(Input.GetKeyDown(key)&&other.CompareTag("Player")&&GameManeger.instant.Temperature<= temper)
        {
            isStart = true;
        }
    }
}
