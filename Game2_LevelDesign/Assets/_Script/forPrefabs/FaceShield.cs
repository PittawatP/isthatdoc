﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceShield : MonoBehaviour
{
    private AudioSource maskpickAS;

    private void Start()
    {
        maskpickAS = GameObject.Find("SFXpickUpMask").GetComponent<AudioSource>();

        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            GameManeger.instant.Faceprotect = 3;
            GameManeger.instant.isFaceOn = true;
            maskpickAS.Play();
            Destroy(gameObject);
        }
    }
}
