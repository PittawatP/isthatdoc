﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class LiftPlatform : MonoBehaviour
{
    [SerializeField]
    private GameObject PlatForm;
    [SerializeField]
    private float Speed;
    [SerializeField]
    private float MaxofRange;
    [SerializeField]
    private float timeDelay;
    private float startTime;
    private bool start=false;
    private Rigidbody rb;
    // Start is called before the first frame update
    private void Start()
    {
        MaxofRange += transform.position.y;
        startTime = timeDelay;
        rb = GetComponent<Rigidbody>();
        rb.useGravity = false;
        rb.mass = 20;
    }
    private void Update()
    {
        if (start)
            if (transform.position.y < MaxofRange)
            {
                rb.velocity += (Vector3.up * Speed * Time.deltaTime);

            }
            else {   
                start = false;
                rb.constraints = RigidbodyConstraints.FreezePositionY;
                rb.constraints = RigidbodyConstraints.FreezeRotation;
            }
        if(transform.position.y>=MaxofRange)
        {
            transform.position=new Vector3(transform.position.x, MaxofRange,transform.position.z);
        }
             
              



    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (timeDelay > 0)
            {
                timeDelay -= Time.deltaTime;
                rb.constraints = RigidbodyConstraints.FreezePositionY;
                rb.constraints = RigidbodyConstraints.FreezeRotation;
            }
            else
            {
                start = true;
                rb.constraints = RigidbodyConstraints.FreezePositionX;
                rb.constraints = RigidbodyConstraints.FreezePositionZ;
                rb.constraints = RigidbodyConstraints.FreezeRotation;

            }
            
        }
     
    }

}
