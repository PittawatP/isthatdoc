﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PillBox : MonoBehaviour
{
    [SerializeField]
    private int PlusPill = 1;
    private AudioSource pillpickAS;

    private void Start()
    {
        pillpickAS = GameObject.Find("SFXpickUpPill").GetComponent<AudioSource>();


    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            pillpickAS.Play();
            Destroy(gameObject);
            GameManeger.instant.Pill += PlusPill;
        }
    }
}
