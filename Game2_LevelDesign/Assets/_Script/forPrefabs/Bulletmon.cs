﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bulletmon : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") )
        {
            if (GameManeger.instant.isFaceOn && GameManeger.instant.Faceprotect > 0)
            {
                GameManeger.instant.Faceprotect--;
            }
            else
            {
                GameManeger.instant.HPPlayer -= 30;
                GameManeger.instant.Temperature++;
            }
            Destroy(gameObject);
        }
        if( other.CompareTag("Wall") || other.CompareTag("Ground"))
        {
            Destroy(gameObject);
        }
    }
}
