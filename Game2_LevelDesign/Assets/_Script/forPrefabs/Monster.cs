﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : MonoBehaviour
{
    [SerializeField]
    private int HP=4;
    [SerializeField]
    private Vector3 Movement;
    [SerializeField]
    private float Speed;
    [SerializeField]
    private float bulletSpeed;
    private Rigidbody rb;
    [SerializeField]
    private int MaxRange=5;

    private Vector3 Pos;
    private GameObject Bullet;
    public GameObject Virus;
    //public GameObject E_bullet;
    [SerializeField]
    private bool Range = false;
    [SerializeField]
    private bool Melee = false;
    [SerializeField]
    private bool Boss = false;
    private bool isAttack = false;

    public BoxCollider HitBox;
    [SerializeField]
    private LayerMask PlayerLayer;

    [SerializeField]
    private GameObject Target;
    [SerializeField]
    private int DMG = 30;

    [SerializeField]
    private float Delay = 1;
    [SerializeField]
    private float StarDelay = 1;
    [SerializeField]
    private int Score;
    [SerializeField]
    ParticleSystem vomit;
    [SerializeField]
    ParticleSystem Splash;

    [SerializeField]
    ParticleSystem VirusSpawnPS;

    private Animator ani;

    private bool Filp = false;

    [SerializeField]
    private AudioClip vomitAC;
    [SerializeField]
    private AudioSource vomitAS;
    public Material mat;

    [SerializeField]
    private Vector3 OffSet;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Pos = transform.position;
        ani = GetComponent<Animator>();
        Delay = StarDelay;
        Target = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {

        if (!GameManeger.instant.isDead && !GameManeger.instant.isPause)
        {
            if (HP <= 0)
            {
                GameManeger.instant.Score += Score;
                Destroy(gameObject);

            }

            if (Range)
            {
                RangeMode();
            }
            if (Melee)
            {
                move();
            }
            if (Boss)
            {
                move();
                SpawnVirus();
                if (HP <= 0)
                {
                    GameManeger.instant.IsGameEnd = true;
                }
            }

            if (Range)
            {

                if (Filp)
                {
                    transform.rotation = Quaternion.Euler(0, 90, 0);
                }
                else
                    transform.rotation = Quaternion.Euler(0, -90, 0);

                if (transform.position.x > Target.transform.position.x)
                {
                    Filp = false;
                }
                else Filp = true;
            }
            MyCollisions();
        }

    }
 
    private void RangeMode()
    {
        if(isAttack)
        {
           if(Delay<0)
            {
                //GameObject go_bullet = Instantiate(E_bullet, new Vector3(transform.position.x, transform.position.y , transform.position.z) + OffSet, Quaternion.identity);
                GameObject go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                go.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                
                SphereCollider coli = go.GetComponent<SphereCollider>();
                coli.isTrigger = true;
                Rigidbody Rb = go.AddComponent<Rigidbody>();
                
                Rb.useGravity = false;
                go.transform.position = new Vector3(this.transform.position.x, this.transform.position.y , this.transform.position.z)+ OffSet;
                go.AddComponent<Bulletmon>();
                go.gameObject.GetComponent<MeshRenderer>().material = mat;
                Rb.AddForce((Target.transform.position - transform.position).normalized * bulletSpeed, ForceMode.Impulse);
                Destroy(go, 3);

                vomit.Play();

                vomitAS.PlayOneShot(vomitAC);

                //Debug.Log(1);
                ani.SetTrigger("isAttack");
                Invoke("setAttacktoFalse", 1f);
                Delay = StarDelay;

            }
            else
            {
                Delay -= Time.deltaTime;
               
            }
        }
        else Delay = StarDelay;




    }


    void MyCollisions()
    {
        //Use the OverlapBox to detect if there are any other colliders within this box area.
        //Use the GameObject's centre, half the size (as a radius) and rotation. This creates an invisible box around your GameObject.
        Collider[] hitColliders = Physics.OverlapBox(HitBox.transform.position, HitBox.transform.localScale /2, Quaternion.identity, PlayerLayer);
        int i = 0;
        //Check when there is a new collider coming into contact with the box
        while (i < hitColliders.Length)
        {
           
            if (hitColliders[i].CompareTag("Player"))
            {
                isAttack = true;

               
            }
        
              
                //Increase the number of Colliders in the array
                i++;
        }
        
    }
    public void move()
    {
        rb.MovePosition(rb.position + Movement * Speed * Time.deltaTime);
       
        if(rb.position.x>Pos.x+MaxRange||rb.position.x<Pos.x-MaxRange)
        {
            Movement = -Movement;
        }

    }
    void setAttacktoFalse() {
        isAttack = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        
       


    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            HP--;
            Splash.Play();
        } if (collision.gameObject.CompareTag("Player"))
        {
            if (GameManeger.instant.Faceprotect > 0 && GameManeger.instant.isFaceOn)
            {
                GameManeger.instant.Faceprotect--;
            }
            else
            {
                GameManeger.instant.Temperature++;
                if (Melee||Boss)
                {
                    GameManeger.instant.HPPlayer -= DMG;
                }
            }
            Debug.Log(1);
        }
    }

    private void SpawnVirus()
    {
        if (isAttack)
        {
            if (Delay <= 0)
            {
                int x = Random.Range(-5, 5);
                GameObject go =  Instantiate(Virus,new Vector3(x+transform.position.x-10,transform.position.y+10,0),Quaternion.identity);
                GameObject go2 = Instantiate(Virus, new Vector3(x + transform.position.x + 10, transform.position.y + 10, 0), Quaternion.identity);
                VirusSpawnPS.Play();

                

                Invoke("setAttacktoFalse", 1f);
                Delay = StarDelay;
                Destroy(go, 15);
                Destroy(go2, 15);

            }
            else
            {
                Delay -= Time.deltaTime;

            }
        }

    }

    
}
