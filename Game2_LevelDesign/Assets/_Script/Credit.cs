﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
public class Credit : MonoBehaviour, IPointerEnterHandler
{
    
    [SerializeField] private Button _backButton;

    AudioSource audiosourceButtomUI;
    [SerializeField] AudioClip audioclipHoldOver;
    private void Start()
    {
        this.audiosourceButtomUI = this.gameObject.AddComponent<AudioSource>();
        this.audiosourceButtomUI.outputAudioMixerGroup = SoundManager.Instance.Mixer.FindMatchingGroups("MasterSFXVolume")[0];
        _backButton.onClick.AddListener(delegate { BackToMainMenuButtonClick(_backButton); });
        //SceneMainMenu
    }


  
    public void BackToMainMenuButtonClick(Button button)
    {

        SceneManager.LoadScene("MainMenu");
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (audiosourceButtomUI.isPlaying)
            audiosourceButtomUI.Stop();

        audiosourceButtomUI.PlayOneShot(audioclipHoldOver);
    }
}

