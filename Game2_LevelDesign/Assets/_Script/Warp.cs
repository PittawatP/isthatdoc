﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Warp : MonoBehaviour
{
    public bool ToLV2;
    public bool ToLv3;

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {if(ToLV2)
            {
                GameManeger.instant.lv2 = true;
                SceneManager.LoadScene("Level2");

            }
            if (ToLv3)
            {
                GameManeger.instant.lv3 = true;
                SceneManager.LoadScene("Level3");
            }
             
        }
    }
}
