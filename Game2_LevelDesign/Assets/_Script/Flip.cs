﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flip : MonoBehaviour
{
    private bool Filp;
    private GameObject Target;
    // Start is called before the first frame update

    void Start()
    {
        Target = GameObject.FindGameObjectWithTag("Player");
    }
    // Update is called once per frame
    void Update()
    {

        if (Filp)
        {
            transform.rotation = Quaternion.Euler(0, -90, 0);
        }
        else
            transform.rotation = Quaternion.Euler(0, 90, 0);

        if (transform.position.x > Target.transform.position.x)
        {
            Filp = false;
        }
        else Filp = true;
    }
}
