﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class SelectLv : MonoBehaviour, IPointerEnterHandler
{
    [SerializeField] Button _Stage1;
    [SerializeField] Button _Stage2;
    [SerializeField] Button _Stage3;

    [SerializeField] Button _backButton;

    AudioSource audiosourceButtomUI;
    [SerializeField] AudioClip audioclipHoldOver;
    // Start is called before the first frame update
    void Start()
    {
        this.audiosourceButtomUI = this.gameObject.AddComponent<AudioSource>();
        this.audiosourceButtomUI.outputAudioMixerGroup = SoundManager.Instance.Mixer.FindMatchingGroups("MasterSFXVolume")[0];
        if (_Stage1!=null)
        _Stage1.onClick.AddListener(delegate { To_Stage1(_Stage1); });
    if(_Stage2!=null)
        _Stage2.onClick.AddListener(delegate { To_Stage2(_Stage2); });
    if(_Stage3!=null)
        _Stage3.onClick.AddListener(delegate { To_Stage3(_Stage3); });

        _backButton.onClick.AddListener(delegate { BackToMainMenuButtonClick(_backButton); });
    }
    private void Update()
    {
        if (!GameManeger.instant.lv2)
            _Stage2.interactable = false;
        else
            _Stage2.interactable = true;
        if (!GameManeger.instant.lv3)
            _Stage3.interactable = false;
        else
            _Stage3.interactable = true;
    }

    // Update is called once per frame
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (audiosourceButtomUI.isPlaying)
            audiosourceButtomUI.Stop();

        audiosourceButtomUI.PlayOneShot(audioclipHoldOver);
    }
    public void BackToMainMenuButtonClick(Button button)
    {
      
        SceneManager.LoadScene("MainMenu");
    }
    public void To_Stage1(Button button)
    {
        GameManeger.instant.GameStart = true;
        SceneManager.LoadScene("Level1");
    }
    public void To_Stage2(Button button)
    {
        GameManeger.instant.GameStart = true;
        SceneManager.LoadScene("Level2");
    }
    public void To_Stage3(Button button)
    {
        GameManeger.instant.GameStart = true;
        SceneManager.LoadScene("Level3");
    }


}

