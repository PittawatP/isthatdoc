﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
public class Option : MonoBehaviour, IPointerEnterHandler
{
  
    [SerializeField] Toggle _toggleMusic;
    [SerializeField] Toggle _toggleSFX;

    [SerializeField] Button _backButton;
    AudioSource audiosourceButtomUI;
    [SerializeField] AudioClip audioclipHoldOver;

    private void Start()
    {
        this.audiosourceButtomUI = this.gameObject.AddComponent<AudioSource>();
        this.audiosourceButtomUI.outputAudioMixerGroup = SoundManager.Instance.Mixer.FindMatchingGroups("MasterSFXVolume")[0];

        _toggleMusic.isOn = GameManeger.instant.MusicEnabled;
        _toggleSFX.isOn = GameManeger.instant.SFXEnabled;
       
        _toggleMusic.onValueChanged.AddListener(delegate { OnToggleMusic(_toggleMusic); });
        _toggleSFX.onValueChanged.AddListener(delegate { OnToggleSFX(_toggleSFX); });
        _backButton.onClick.AddListener(delegate { BackToMainMenuButtonClick(_backButton); });
        //SceneMainMenu
    }


    public void OnToggleMusic(Toggle toggle)
    {
        GameManeger.instant.MusicEnabled = _toggleMusic.isOn;
        if (GameManeger.instant.MusicEnabled)
            SoundManager.Instance.MusicVolume = SoundManager.Instance.MusicVolumeDefault;
        else
            SoundManager.Instance.MusicVolume = SoundManager.MUTE_VOLUME;
    }
    public void OnToggleSFX(Toggle toggle)
    {
        GameManeger.instant.SFXEnabled = _toggleSFX.isOn;
        if (GameManeger.instant.SFXEnabled)
            SoundManager.Instance.MasterSFXVolume = SoundManager.Instance.MasterSFXVolumeDefault;
        else
            SoundManager.Instance.MasterSFXVolume = SoundManager.MUTE_VOLUME;

    }
    public void BackToMainMenuButtonClick(Button button)
    {

        SceneManager.LoadScene("MainMenu");
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (audiosourceButtomUI.isPlaying)
            audiosourceButtomUI.Stop();

        audiosourceButtomUI.PlayOneShot(audioclipHoldOver);
    }
}
