﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

public class Mouse : MonoBehaviour
{
    private PlayerController user;
    public Camera cam;
    Vector3 worldPosition;
    public GameObject ball;

    public GameObject bullet;

    public float Speed;
    private Vector3 lookDir;
    [SerializeField]
    private Vector3 offset;
    [SerializeField]
    private GameObject gun;
    public GameObject target;

    public ParticleSystem gunEffect;


    [Range(-360, 360)]
    [SerializeField]
    private float minimun;

    [Range(-360, 360)]
    [SerializeField]
    private float maximun;

    public float mouseDistanct;

    [SerializeField] AudioClip audioclipShoot;
    
    [SerializeField] AudioSource ShootingSFX;
    
    void Update()
    {
        if (!GameManeger.instant.isPause)
        {
            user = GetComponent<PlayerController>();

            if (Input.GetMouseButtonDown(0) && GameManeger.instant.Ammo > 0)
            {
                gunEffect.Play();
                ShootingSFX.PlayOneShot(audioclipShoot);
                GameManeger.instant.Ammo--;

                GameObject go = Instantiate(bullet, gun.transform.position, Quaternion.identity);
                Rigidbody Rb = go.GetComponent<Rigidbody>();
                Rb.AddForce((target.transform.position - gun.transform.position).normalized * Speed, ForceMode.Impulse);
                go.AddComponent<Bullet>();
                Destroy(go, 2f);
            }

        }

    }
    private void FixedUpdate()
    {
        if (!GameManeger.instant.isPause)
        {


            //  ball.transform.position = new Vector3((transform.position.x + worldPosition.x - cam.transform.position.x),
            //       transform.position.y + worldPosition.y - cam.transform.position.y,
            //        transform.position.z);

            Vector3 mousePos = Input.mousePosition;
            mousePos.z = Camera.main.nearClipPlane;
            worldPosition = Camera.main.ScreenToWorldPoint(mousePos);
            worldPosition.z = transform.position.z;
            // Debug.Log(worldPosition);


            lookDir = worldPosition - transform.position;
            float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90f;
            if (lookDir.x > mouseDistanct && user.Diraction == 1)
            {
                if (angle > minimun && angle < maximun)
                {
                    ball.transform.rotation = Quaternion.Euler(0, 0, angle);

                }

            }

            if (lookDir.x < mouseDistanct && user.Diraction == -1)
            {
                if (angle > minimun && angle < maximun)
                {
                    ball.transform.rotation = Quaternion.Euler(0, 180, -angle);

                }

            }
        }
    }
}
