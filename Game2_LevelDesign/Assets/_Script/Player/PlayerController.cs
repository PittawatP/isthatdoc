﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour
{
    private Rigidbody rb;
    [SerializeField]
    private float speed;
    public GameObject Prefabs;
    public bool canJump = true;
    [SerializeField]
    AudioClip walkAC;
    [SerializeField]
    AudioSource walkAS;
    [SerializeField]
    AudioClip JumpAC;
    [SerializeField]
    AudioSource JumpAS;
    [SerializeField]
    AudioSource Pilluse;
    [SerializeField]
    AudioClip PilluseAC;

    public bool isplay;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();

        // get the transform of the main camera
        if (Camera.main != null)
        {
            m_Cam = Camera.main.transform;
        }
        else
        {
            Debug.LogWarning(
                "Warning: no main camera found. Third person character needs a Camera tagged \"MainCamera\", for camera-relative controls.", gameObject);
            // we use self-relative controls in this case, which probably isn't what the user wants, but hey, we warned them!
        }

        // get the third person character ( this should never be null due to require component )
        m_Character = GetComponent<ThirdPersonCharacter>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!m_Jump)
        {
            m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");

 
        }
 
        //   float h = Input.GetAxisRaw("Horizontal");

        //  Vector3 Movement = new Vector3(h, 0, 0);
        //   rb.MovePosition(rb.position+Movement*speed*Time.deltaTime);
        if (!GameManeger.instant.isDead)
        {


            if (Input.GetKeyDown(KeyCode.Space) && canJump)
            {
               // rb.AddForce(Vector3.up * speed, ForceMode.Impulse);
                //canJump = false;
            }
            if (Input.GetKeyDown(KeyCode.Space)&&canJump)
            {
                walkAS.Stop();
                JumpAS.PlayOneShot(JumpAC);
                canJump = false;
            }

            if (Input.GetKeyDown(KeyCode.Q) && GameManeger.instant.Pill > 0)
            {
                GameManeger.instant.HPPlayer += 25;
                GameManeger.instant.Temperature--;
                Pilluse.PlayOneShot(PilluseAC);
                GameManeger.instant.Pill--;
            }
            if (GameManeger.instant.isFaceOn)
            {
                Prefabs.SetActive(true);
            }
            else Prefabs.SetActive(false);
        }

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            canJump = true;
            
        }
        if (collision.gameObject.CompareTag("Monmelee"))
        {
            GameManeger.instant.HPPlayer--;
        }
    }
    private ThirdPersonCharacter m_Character; // A reference to the ThirdPersonCharacter on the object
    private Transform m_Cam;                  // A reference to the main camera in the scenes transform
    private Vector3 m_CamForward;             // The current forward direction of the camera
    private Vector3 m_Move;
    private bool m_Jump;                      // the world-relative desired move direction, calculated from the camForward and user input.
    public int Diraction;



    private void FixedUpdate()
    {
        if (!GameManeger.instant.isPause && !GameManeger.instant.isDead)
        {
            usercontrol();
        }
    }

    // Fixed update is called in sync with physics
    private void usercontrol()
    {
        // read inputs
        float h = CrossPlatformInputManager.GetAxis("Horizontal");
        float v = 0;
        bool crouch = false;
        if (h > 0)
        {
            Diraction = 1;
            transform.rotation = Quaternion.Euler(0, 90, 0);
        }

        if (h < 0)
        {
            Diraction = -1;
            transform.rotation = Quaternion.Euler(0, -90, 0);

        }
        if (h == 0)
        {
            walkAS.Play();
        }
        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.D))
        {
            walkAS.Play();
        }
        //if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
        //{
        //    walkAS.Stop();
        //}

        // calculate move direction to pass to character
        if (m_Cam != null)
        {
            // calculate camera relative direction to move:
            m_CamForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 0, 1)).normalized;
            m_Move = v * m_CamForward + h * m_Cam.right;
        }
        else
        {
            // we use world-relative directions in the case of no main camera
            m_Move = v * Vector3.forward + h * Vector3.right;
        }
#if !MOBILE_INPUT
        // walk speed multiplier
        if (Input.GetKey(KeyCode.LeftShift)) m_Move *= 0.5f;
#endif

        // pass all parameters to the character control script
        m_Character.Move(m_Move, crouch, m_Jump);
        m_Jump = false;
    }


}

