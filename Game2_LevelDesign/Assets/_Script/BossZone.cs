﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossZone : MonoBehaviour
{
    [SerializeField]
    bool isInbossZone;

    public Collider BossZoneBarrier;
    public Collider BossZoneBarrier2;
    [SerializeField]
    private GameObject BossOBJ;

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            isInbossZone = true;
            BossZoneBarrier.isTrigger = false;
            BossZoneBarrier2.isTrigger = false;
        }
        
        
    }
    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            isInbossZone = false;
        } 
        
    }

    private void Update()
    {
        if(!GameManeger.instant.IsGameEnd)
        {
            if (isInbossZone && BossOBJ.transform.position.y > 35)
            {
                BossOBJ.transform.position -= new Vector3(0,5,0) * Time.deltaTime;
            }
        }
        if (GameManeger.instant.HPPlayer <= 0)
        {

            if (GameManeger.instant.Life > 0)
            {
                isInbossZone = false;
            }
            
        }

        if(!isInbossZone)
        {
            BossZoneBarrier.isTrigger = true;
            BossZoneBarrier2.isTrigger = true;
        }
    }

}
